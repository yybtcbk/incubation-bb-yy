// ==UserScript==
// @name        futaba_dice_kanmusu_append
// @version     1.0.0.20170201
// @description ふたばのダイス機能の結果に対応する艦娘名を付与する
// @namespace   http://www.2chan.net/
// @author      Futaba Toshiaki / yybtcbk
// @license     MIT https://opensource.org/licenses/mit-license.php
// @grant       none
// @downloadURL https://bitbucket.org/yybtcbk/incubation-bb-yy/raw/master/js/futaba-userscripts/futaba_dice_kanmusu_append.user.js
// @include     http://dec.2chan.net/60/res/*.htm
// @include     https://dev.ftbucket.info/scrapshot/ftb/cont/dec.2chan.net_60_res_*/index.htm
// @include     http://futabalog.com/thread/*
// ==/UserScript==

(function(){

//**********************************************************************
//************** CONSTANTS ： 必要・好みに応じて編集可
//**********************************************************************

var UNKNOWN_NAME = '???';       //下記艦娘リストに無い場合の表記
var ADD_TEXT_COLOR = '#0000FF'; //艦娘名付記の文字色
var KANMUSU_LIST = {
//======= 艦娘が追加されたら以下の艦娘リストに追記する =======
//参照： http://wikiwiki.jp/kancolle/?%B4%CF%CC%BC%A5%AB%A1%BC%A5%C9%B0%EC%CD%F7
    1 : '長門',
    2 : '陸奥',
    3 : '伊勢',
    4 : '日向',
    5 : '雪風',
    6 : '赤城',
    7 : '加賀',
    8 : '蒼龍',
    9 : '飛龍',
   10 : '島風',
   11 : '吹雪',
   12 : '白雪',
   13 : '初雪',
   14 : '深雪',
   15 : '叢雲',
   16 : '磯波',
   17 : '綾波',
   18 : '敷波',
   19 : '大井',
   20 : '北上',
   21 : '金剛',
   22 : '比叡',
   23 : '榛名',
   24 : '霧島',
   25 : '鳳翔',
   26 : '扶桑',
   27 : '山城',
   28 : '天龍',
   29 : '龍田',
   30 : '龍驤',
   31 : '睦月',
   32 : '如月',
   33 : '皐月',
   34 : '文月',
   35 : '長月',
   36 : '菊月',
   37 : '三日月',
   38 : '望月',
   39 : '球磨',
   40 : '多摩',
   41 : '木曾',
   42 : '長良',
   43 : '五十鈴',
   44 : '名取',
   45 : '由良',
   46 : '川内',
   47 : '神通',
   48 : '那珂',
   49 : '千歳',
   50 : '千代田',
   51 : '最上',
   52 : '古鷹',
   53 : '加古',
   54 : '青葉',
   55 : '妙高',
   56 : '那智',
   57 : '足柄',
   58 : '羽黒',
   59 : '高雄',
   60 : '愛宕',
   61 : '摩耶',
   62 : '鳥海',
   63 : '利根',
   64 : '筑摩',
   65 : '飛鷹',
   66 : '隼鷹',
   67 : '朧',
   68 : '曙',
   69 : '漣',
   70 : '潮',
   71 : '暁',
   72 : '響',
   73 : '雷',
   74 : '電',
   75 : '初春',
   76 : '子日',
   77 : '若葉',
   78 : '初霜',
   79 : '白露',
   80 : '時雨',
   81 : '村雨',
   82 : '夕立',
   83 : '五月雨',
   84 : '涼風',
   85 : '朝潮',
   86 : '大潮',
   87 : '満潮',
   88 : '荒潮',
   89 : '霰',
   90 : '霞',
   91 : '陽炎',
   92 : '不知火',
   93 : '黒潮',
   94 : '祥鳳',
   95 : '千歳改',
   96 : '千代田改',
   97 : '大井改',
   98 : '北上改',
   99 : '千歳甲',
  100 : '千代田甲',
  101 : '最上改',
  102 : '伊勢改',
  103 : '日向改',
  104 : '千歳航',
  105 : '千代田航',
  106 : '翔鶴',
  107 : '瑞鶴',
  108 : '瑞鶴改',
  109 : '鬼怒',
  110 : '阿武隈',
  111 : '夕張',
  112 : '瑞鳳',
  113 : '瑞鳳改',
  114 : '大井改二',
  115 : '北上改二',
  116 : '三隈',
  117 : '三隈改',
  118 : '初風',
  119 : '舞風',
  120 : '衣笠',
  121 : '千歳航改二',
  122 : '千代田航改二',
  123 : '伊19',
  124 : '鈴谷',
  125 : '熊野',
  126 : '伊168',
  127 : '伊58',
  128 : '伊8',
  129 : '鈴谷改',
  130 : '熊野改',
  131 : '大和',
  132 : '秋雲',
  133 : '夕雲',
  134 : '巻雲',
  135 : '長波',
  136 : '大和改',
  137 : '阿賀野',
  138 : '能代',
  139 : '矢矧',
  140 : '酒匂',
  141 : '五十鈴改二',
  142 : '衣笠改二',
  143 : '武蔵',
  144 : '夕立改二',
  145 : '時雨改二',
  146 : '木曾改二',
  147 : 'Верный',
  148 : '武蔵改',
  149 : '金剛改二',
  150 : '比叡改二',
  151 : '榛名改二',
  152 : '霧島改二',
  153 : '大鳳',
  154 : '香取',
  155 : '伊401',
  156 : '大鳳改',
  157 : '龍驤改二',
  158 : '川内改二',
  159 : '神通改二',
  160 : '那珂改二',
  161 : 'あきつ丸',
  162 : '未実装',
  163 : 'まるゆ',
  164 : '弥生',
  165 : '卯月',
  166 : 'あきつ丸改',
  167 : '磯風',
  168 : '浦風',
  169 : '谷風',
  170 : '浜風',
  171 : 'Bismarck',
  172 : 'Bismarck改',
  173 : 'Bismarck zwei',
  174 : 'Z1',
  175 : 'Z3',
  176 : 'Prinz Eugen',
  177 : 'Prinz Eugen改',
  178 : 'Bismarck drei',
  179 : 'Z1 zwei',
  180 : 'Z3 zwei',
  181 : '天津風',
  182 : '明石',
  183 : '大淀',
  184 : '大鯨',
  185 : '龍鳳',
  186 : '時津風',
  187 : '明石改',
  188 : '利根改二',
  189 : '筑摩改二',
  190 : '龍鳳改',
  191 : '妙高改二',
  192 : '那智改二',
  193 : '足柄改二',
  194 : '羽黒改二',
  195 : '綾波改二',
  196 : '飛龍改二',
  197 : '蒼龍改二',
  198 : '未実装',
  199 : '大潮改二',
  200 : '阿武隈改二',
  201 : '雲龍',
  202 : '天城',
  203 : '葛城',
  204 : '初春改二',
  205 : '春雨',
  206 : '雲龍改',
  207 : '潮改二',
  208 : '隼鷹改二',
  209 : '早霜',
  210 : '清霜',
  211 : '扶桑改二',
  212 : '山城改二',
  213 : '朝雲',
  214 : '山雲',
  215 : '野分',
  216 : '古鷹改二',
  217 : '加古改二',
  218 : '皐月改二',
  219 : '初霜改二',
  220 : '叢雲改二',
  221 : '秋月',
  222 : '照月',
  223 : '初月',
  224 : '高波',
  225 : '朝霜',
  226 : '吹雪改二',
  227 : '鳥海改二',
  228 : '摩耶改二',
  229 : '天城改',
  230 : '葛城改',
  231 : 'U-511',
  232 : 'Graf Zeppelin',
  233 : 'Saratoga',
  234 : '睦月改二',
  235 : '如月改二',
  236 : '呂500',
  237 : '暁改二',
  238 : 'Saratoga改',
  239 : 'Warspite',
  240 : 'Iowa',
  241 : 'Littorio',
  242 : 'Roma',
  243 : 'Libeccio',
  244 : 'Aquila',
  245 : '秋津洲',
  246 : 'Italia',
  247 : 'Roma改',
  248 : 'Zara',
  249 : 'Pola',
  250 : '秋津洲改',
  251 : '瑞穂',
  252 : '沖波',
  253 : '風雲',
  254 : '嵐',
  255 : '萩風',
  256 : '親潮',
  257 : '山風',
  258 : '海風',
  259 : '江風',
  260 : '速吸',
  261 : '翔鶴改二',
  262 : '瑞鶴改二',
  263 : '朝潮改二',
  264 : '霞改二',
  265 : '鹿島',
  266 : '翔鶴改二甲',
  267 : '瑞鶴改二甲',
  268 : '朝潮改二丁',
  269 : '江風改二',
  270 : '霞改二乙',
  271 : '神風',
  272 : '朝風',
  273 : '春風',
  274 : '未実装',
  275 : '未実装',
  276 : '神風改',
  277 : '未実装',
  278 : '未実装',
  279 : '未実装',
  280 : '未実装',
  281 : '水無月',
  282 : '未実装',
  283 : '伊26',
  284 : '未実装',
  285 : '未実装',
  286 : '浦波',
  287 : '鬼怒改二',
  288 : '未実装',
  289 : '未実装',
  290 : '荒潮改二',
  291 : 'Commandant Teste',
  292 : '未実装',
  293 : '未実装',
  294 : '未実装',
  295 : '未実装',
  296 : '未実装',
  297 : '未実装',
  298 : '未実装',
  299 : '未実装',
  300 : '未実装',
  
  99000 : 'イオナ',
  99001 : 'タカオ',
  99002 : 'ハルナ',
//======= 艦娘リスト終わり =======
'end':''};
delete KANMUSU_LIST.end; //makes copy-and-pasting of above list easier

//正規表現マッチ：ふたば側の仕様変更が無い限り弄らない事
var MATCH_PATTERN = /(dice[0-9]+d[0-9]+=(?:<font.*?>)?)([0-9 ]+)(\\\([0-9]+\\\)(?:<\/font>)?)/;

//**********************************************************************
//*********** CODE BLOCK ： 以下はよくわからないなら弄らない事
//**********************************************************************

var els = getResElems();

for(var i=0, len=els.length; i < len; i++) {
  var el = els[i];
  
  var tArr = escapeRegExp(el.innerHTML).match(MATCH_PATTERN);//[0] is whole match, [1] is "diceNdN=", 
                                                             //[2] is list of dice, [3] is "(N)"
  if(tArr===null) { //i.e. if dice text not found
    continue;       //continue to next blockquote (== next res.)
  }
  var repMatchStr = tArr[1] + appendKanmusuNames(tArr[2]) + tArr[3];
  el.innerHTML = unescapeRegExp(escapeRegExp(el.innerHTML).replace(MATCH_PATTERN, repMatchStr));
}

//*************************** FUNCTIONS ***************************

//レスのタグブロックのArrayを返す。ふたば純正のタグ形式以外のサイトも@includeしたいならここを弄る必要あり。
function getResElems() {
  var fullPageUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
  if(/^http:\/\/futabalog\.com\/thread\//.test(fullPageUrl)) {
    return document.getElementsByClassName("res");
  } else {
    return document.getElementsByTagName("blockquote"); //ふたば純正形式
  }
}

//'192 168 ' 等の文字列の数値それぞれに対応する艦娘名をカッコ付きで付記して返す
function appendKanmusuNames(kmNoStr) {
  var kmNoArr = kmNoStr.split(' ');
  var kmName = '', kmNo = 0;
  var repText = '';
  for(var j=0, lenj=kmNoArr.length; j < lenj; j++) {
    kmNo = kmNoArr[j];
    if(!(kmNo in KANMUSU_LIST)) {
      kmName = UNKNOWN_NAME;
    } else {
      kmName = KANMUSU_LIST[kmNo];
    }
    if(kmNo!=='') {
      repText += kmNo + '<font color=\'' + ADD_TEXT_COLOR + '\'>(' + kmName + ')</font> ';
    }
  }
  return repText;
}

//function taken from https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions
function escapeRegExp(string){
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

//improvised from above
function unescapeRegExp(string){
  return string.replace(/\\([.*+?^${}()|[\]\\])/g, '$1'); // $1 is the captured symbol following the \
}

})();
