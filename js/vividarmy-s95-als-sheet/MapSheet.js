/**
 * マップシートの関係処理用クラス
 *  20190605 yybtcbk 初版：編集時処理 を移行
 */
var MapSheet = function(){
  //======= private vars and actions upon construction =======
//writeLog("dbg:Map","Map-create");
  
  //======= privileged functions =======
  /**
   * 編集時処理
   *  20190604 yybtcbk 初版：チャットエリアの書き込み日時の自動記述処理に対応
   * @param {EventObject} e - onEdit(e) 経由でもらったイベントオブジェクト
   */
  this.processOnEdit = function(e){
    var eRow = e.range.getRow();
    var eCol = e.range.getColumn();
    if((eCol == MapSheet.CHAT_COL) && (eRow >= MapSheet.CHAT_ROW_START)){ //チャットエリアの修正の場合のみ、日時処理を行う
      /* チャットエリアの書き込み日時の自動記述処理
         20190604 yybtcbk 初版：GAS独自関数の挙動が安定しないので、こっちにチャットの日時処理を移行する。
         　 仕様１：チャットエリア（関係変数は直しやすいように上にグローバル化）に新規に書き込むと、対応する日時が自動記述される。
         　 仕様２：記述済みの行の内容を修正した場合は、日時記述は更新されない。
         　 仕様３：記述内容を削除した場合は、日時記述も削除される。
         　 　　　　ただし、複数行削除の場合は、残る行が出るので、その場合は以下のどちらかで運用回避とする。（対応めどい）
            　  　　a) 一行ずつ消す（一般ユーザーでも可能）
              　　　b) 日付記述を直接消す（編集権限ユーザーの場合）
      　　　補足１：日付記述のセルに関数を埋め込まないため、コピペ等が楽。
           補足２：ただし既存のonEdit()を直接触っているので、お行儀はよろしくない。（でもめどいのでこれで……）
         20190605 yybtcbk 以降に伴い少し手直し
      */
//      var text = e.value; //チャットの編集内容を取得ｘｘｘ中身が空になると、「{oldValue=あああ}」とか返す？ので、使わない
      var text = e.range.getValue(); //チャットの編集内容を取得
      var datetimeCell = e.source.getActiveSheet().getRange(eRow,eCol+MapSheet.CHAT_COL_NUM); //日時セル取得
    
      if(text===""){ //チャットの編集内容がない場合
        datetimeCell.setValue(""); //日時クリアしてから
        return;                    //戻る
      }
      if(datetimeCell.getValue()!=="") return; //日時記述がすでにあるなら、なにもしないで戻る
      //現在日時を書き込む
      var timestamp = Utilities.formatDate(new Date(), "JST", "M/d HH:mm"); //though "JST" is probably deprecated...?
      datetimeCell.setValue(timestamp); //日時書き込み
//writeLog("dbg:Map","cellA1="+e.range.getA1Notation()+",timestamp="+timestamp);
    }
  };
};
//======= (static) members =======
/**
 * チャットエリアの列の数値
 * @type {number}
 */
MapSheet.CHAT_COL = 28;
/**
 * チャットエリアの開始行の数値
 * @type {number}
 */
MapSheet.CHAT_ROW_START = 8;
/**
 * チャットエリアの列幅の数値
 * @type {number}
 */
MapSheet.CHAT_COL_NUM = 2;
