/**
 * 共通関数の置き場
 */

/*** POLYFILLS ***/
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/EPSILON#Polyfill
if (Number.EPSILON === undefined) {
  Number.EPSILON = Math.pow(2, -52);
}
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/log10#Polyfill
Math.log10 = Math.log10 || function(x) {
  return Math.log(x) * Math.LOG10E;
};
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter#Polyfill
if (!Array.prototype.filter){
  Array.prototype.filter = function(func, thisArg) {
    'use strict';
    if ( ! ((typeof func === 'Function' || typeof func === 'function') && this) )
        throw new TypeError();
    var len = this.length >>> 0,
        res = new Array(len), // preallocate array
        t = this, c = 0, i = -1;
    if (thisArg === undefined){
      while (++i !== len){
        // checks to see if the key was set
        if (i in this){
          if (func(t[i], i, t)){
            res[c++] = t[i];
          }
        }
      }
    }
    else{
      while (++i !== len){
        // checks to see if the key was set
        if (i in this){
          if (func.call(thisArg, t[i], i, t)){
            res[c++] = t[i];
          }
        }
      }
    }
    res.length = c; // shrink down array to proper size
    return res;
  };
}
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map#Polyfill comments stripped
if (!Array.prototype.map) {
  Array.prototype.map = function(callback/*, thisArg*/) {
    var T, A, k;
    if (this == null) {
      throw new TypeError('this is null or not defined');
    }
    var O = Object(this);
    var len = O.length >>> 0;
    if (typeof callback !== 'function') {
      throw new TypeError(callback + ' is not a function');
    }
    if (arguments.length > 1) {
      T = arguments[1];
    }
    A = new Array(len);
    k = 0;
    while (k < len) {
      var kValue, mappedValue;
      if (k in O) {
        kValue = O[k];
        mappedValue = callback.call(T, kValue, k, O);
        A[k] = mappedValue;
      }
      k++;
    }
    return A;
  };
}
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes#Polyfill comments stripped
if (!Array.prototype.includes) {
  Object.defineProperty(Array.prototype, 'includes', {
    value: function(valueToFind, fromIndex) {
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }
      var o = Object(this);
      var len = o.length >>> 0;
      if (len === 0) {
        return false;
      }
      var n = fromIndex | 0;
      var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

      function sameValueZero(x, y) {
        return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
      }

      while (k < len) {
        if (sameValueZero(o[k], valueToFind)) {
          return true;
        }
        k++;
      }
      return false;
    }
  });
}
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/every#Polyfill comments stripped
if (!Array.prototype.every) {
  Array.prototype.every = function(callbackfn, thisArg) {
    'use strict';
    var T, k;
    if (this == null) {
      throw new TypeError('this is null or not defined');
    }
    var O = Object(this);
    var len = O.length >>> 0;
    if (typeof callbackfn !== 'function') {
      throw new TypeError();
    }
    if (arguments.length > 1) {
      T = thisArg;
    }
    k = 0;
    while (k < len) {
      var kValue;
      if (k in O) {
        kValue = O[k];
        var testResult = callbackfn.call(T, kValue, k, O);
        if (!testResult) {
          return false;
        }
      }
      k++;
    }
    return true;
  };
}



/**
 * 渡された内容をログシートにログ書き込みする（タイムスタンプは自動付記）
 *   前提１：logシートの一列目から三列目に書き込む
 *   前提２：一列目は上から空き無く埋まっている
 * @param {string} senderId - 送信者ID
 * @param {string\|Array.<string>} message - ログメッセージ、あるいはその配列
 */  
function writeLog(senderId, message) {
  var LOG_ROW_UB=1001; var LOG_ROW_CHUNKSIZE=200;
//  var logSht = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("log");
  var logSht = getSheetById(SHEETS_DICT.log.gid);
  var logRow = 1; var chunk, tryChunk;
  //find last log
//  while((logSht.getRange(logRow,1).getValue()!=="") && (logRow<999)){
//    logRow++;
//  }
  do{
    tryChunk = LOG_ROW_CHUNKSIZE + Math.min(0,LOG_ROW_UB-LOG_ROW_CHUNKSIZE-logRow+1);
    //cf https://stackoverflow.com/a/17637159/3799649
    chunk = logSht.getRange("A"+logRow+":A"+(logRow+tryChunk-1)).getValues().filter(String).length;
    logRow += chunk;
  }while(chunk==LOG_ROW_CHUNKSIZE && logRow<LOG_ROW_UB);
  //write log
  var timestamp = Utilities.formatDate(new Date(),"JST","YYYY-MM-dd_HH-mm-ss");
  var outArr = []; var outRows;
  if(Array.isArray(message)){ //ログメッセージの配列を渡された場合は、連続で書き出す
    outRows = message.length;
    for(var i=0; i<message.length; i++){
      outArr.push([timestamp,senderId,message[i]]);
    }
  }
  else{ //通常ログメッセージの書き出し
    outRows = 1;
    outArr.push([timestamp,senderId,message]);
  }
  //一気に書き出す
  logSht.getRange(logRow,1,outRows,3).setValues(outArr);
}

/**
 * 指定のgidのシートを返す。
 * cf https://stackoverflow.com/a/51789725/3799649
 * @param {String} gid - シートのgid（URLの#gid=以降の数字と同じ）
 * @return {Sheet} gidに対応するシート、無ければundefined
 */  
function getSheetById(gid){
  for each (var sheet in SpreadsheetApp.getActive().getSheets()) {
    if(sheet.getSheetId()==gid) return sheet;
  }
  return undefined; //シートが見つからなかった場合；いっそthrow Errorすべきかも？
}

/**
 * 渡された値が数値かどうかを判定する。
 * cf http://aoking.hatenablog.jp/entry/20120217/1329452700
 * @param {Object} x - 判定する値
 * @return {boolean} 渡された値が数値判定ならtrue、数値以外（空の場合も含む）ならfalse
 */  
function isNumber(x){
//  return !isNaN(x); //これだけだと弱い奴
  if((typeof(x)!="number") && (typeof(x)!="string")) return false;
  else return ((x==parseFloat(x)) && isFinite(x));
}

/**
 * 指定の小数点以下桁まで四捨五入する。
 *   method1: 浮動小数点演算誤差の部分的修正の試みも含むが、それでもたまに間違える……
 *   method2: eの扱いがめどいけど、精度はマシ。
 * どちらにせよ、表示精度以上の精度は追及しない（できない）。
 * cf https://stackoverflow.com/q/11832914/3799649
 * @param {number} num - 四捨五入の対象となる数値
 * @param {number} dp - 四捨五入する小数点以下桁
 * @return {number} 四捨五入した数値、あるいは無理ならNaN
 */ 
function roundValToDp(num,dp){
  if(!isNumber(num)||!isNumber(dp)) return NaN;
  //＝＝＝method1：εで補正
//  var pow = Math.pow(10,dp);
//  var sign = num<0 ? -2 : +1 ; //try to force round(-0.5) to -1 ...
//  return Math.round( num*pow + Number.EPSILON*pow/2*sign ) / pow;
  
  //＝＝＝method2：指数表記の調整で、誤差を最小化
  // Shift
  var value = num.toString().split('e');
  var offset = num<0 ? -1*Number.EPSILON*Math.pow(10,dp) : 0 ; //try to force round(-0.5) to -1 ...
  value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + dp) : dp))+offset);
  // Shift back
  value = value.toString().split('e');
  return +(value[0] + 'e' + (value[1] ? (+value[1] - dp) : -dp));
}

/*** TESTS ***/
/*
function testIsNumber(){
  logIsNum(1);
  logIsNum(3.14);
  logIsNum(Math.PI);
  logIsNum(-2/3);
  logIsNum(1e99);
  logIsNum("-5e-3");
  logIsNum("abc");
  logIsNum("123abc");
  logIsNum("1.1.1");
  logIsNum("12:12");
  logIsNum("0xFF"); //dubious
  logIsNum(0xFF);
  logIsNum(NaN);
  logIsNum(Infinity);
  logIsNum(0/0);
  logIsNum();
  logIsNum(null);
  logIsNum([]);
  logIsNum({});
  logIsNum(true);
  logIsNum(false);
  logIsNum([1]); //should be false, but log display is misleading
  
  function logIsNum(x){
    Logger.log(x+" isNumber: "+isNumber(x));
  }
}

function testRoundValToDp(){
  logOneRoundValToDp("1.499",0);
  logOneRoundValToDp("1.5",0);
  logOneRoundValToDp("1.4499",1);
  logOneRoundValToDp("1.45",1);
  logOneRoundValToDp("1.44499",2);
  logOneRoundValToDp("1.445",2);
  logOneRoundValToDp("-1.44499",2);
  logOneRoundValToDp("-1.445",2);
  logOneRoundValToDp("-1.44501",2);
  logOneRoundValToDp("-1.05",1);
  logOneRoundValToDp("-1.5",0);
  logOneRoundValToDp("1.234e-2",2);
  logOneRoundValToDp("3.1415926535e5",2);
  logOneRoundValToDp("-2.718281828e-3",7);
  //floating-point errors...
  logDisplayValue("1.005");
  logOneRoundValToDp("1.005",2);
//  logDisplayValue("1.0049999999999997");
//  logOneRoundValToDp("1.0049999999999997",2);
  logDisplayValue("1.00499999999999978");
  logOneRoundValToDp("1.00499999999999978",2);
  logDisplayValue("1.00499999999999979");
  logOneRoundValToDp("1.00499999999999979",2);
//  logDisplayValue("1.00499999999999980");
//  logOneRoundValToDp("1.00499999999999980",2);
  logOneRoundValToDp("-1.005",2);
  logDisplayValue("-1.00499999999999978");
  logOneRoundValToDp("-1.00499999999999978",2);
  logDisplayValue("-1.00499999999999979");
  logOneRoundValToDp("-1.00499999999999979",2);
  logDisplayValue("156893.145");
  logOneRoundValToDp("156893.145",2); // method 1x 156893.14
  logDisplayValue("1.555");
  logOneRoundValToDp("1.555",2);
  logDisplayValue("0.43499999999999994");
  logOneRoundValToDp("0.43499999999999994",2); // method1 x 0.44
  logDisplayValue("1234.00000254495");
  logOneRoundValToDp("1234.00000254495",10); // method1 x 1234.0000025449
  //dubious uses
  logOneRoundValToDp("123.456",-1); //nearest 10 to 120
  logOneRoundValToDp("99499.99999",-3); //nearest 1000 to 99000
  logOneRoundValToDp("1150",-2); //nearest 100 to 1200
  logOneRoundValToDp("1.33",Math.log10(4));//nearest 1/4 to 1.25 : method2 x NaN
  
  function logOneRoundValToDp(numstr,dp){
    Logger.log(numstr+" to "+dp+"dp rounds to "+roundValToDp(+(numstr),dp));
  }
  function logDisplayValue(numstr){
    Logger.log(numstr+" displays as "+(+(numstr)));
  }
}
//*/
