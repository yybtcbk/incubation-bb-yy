/**
 * Alsシート用のスクリプトセット。
 *  詳細は、各関数の説明を参照。
 * @OnlyCurrentDoc
 */
/**
 * @typedef {Object} EventObject
 * @typedef {Object} HTMLElement
 * @typedef {Object} Sheet
 */

//TODO refactoring and make lighter
//TODO perhaps use jquery for a change...?

/**
 * Alsシート（ブック）の各シートの定義辞書(dictionary)
 * keyはシートの簡易ID
 * valueの定義辞書の内容は、そのシートの
 *   gid：gid, URLの「#gid=636442918」の部分の数値
 *   name：シート名（変わる可能性があるので参考使用程度に抑えたい）
 * @type {Object.<String,Object.<String,String\|number>>}
 */
var SHEETS_DICT = {
  map: {
    gid:0,
    name:"マップ",
  },
  lvlCalc: {
    gid:969189818,
    name:"レベルアップ必要金貨",
  },
  log: {
    gid:636442918,
    name:"log",
  },
};
Object.freeze(SHEETS_DICT);

//シングルトン実装めどいので怠ける
//…と思ったけど、GASスクリプト自体が別コールで、ここも毎回初期化されるので、無意味……orz
//var lvc = undefined;
//var chat = undefined;

/* 検証用：通常時はブロックコメントアウトする
function onEdit(e) {
  //throw new Error("check authmode="+e.authMode); //authModeテスト用：編集権限の有無にかかわらず、LIMITEDで動いている
  if(e.range.getRow()>=28) return; //下の検証分を消せるように
  e.source.getSheetByName("マップ").getRange(28,28).setValue("testOnEdit");  //書き込み権限があれば、書き込める
  e.source.getSheetByName("マップ").getRange(28,30).setValue("88/88 88:88"); //書き込み権限がないと、サイレントに失敗し、スクリプト実行もそこで止まる
  e.source.getSheetByName("マップ").getRange(29,28).setValue("testOnEdit2"); //…上で失敗すると、ここまで来ない
  //つまり、日時書き込み、ログ記述、両方とも、当該ロックを外せば、こっちで使えそうだけど、
  //・Simple Trigger だとロック外しの弊害があるかも（あまり無いと思う）
  //・installable trigger だと権限とかクォータとかが問題になってくるかも（大丈夫そう）
  //なので、後者のまま様子を見つつ、必要なら前者に移行する予定。
}
//*/
//* ローカルデバッグ用：プロダクション時にはブロックコメントアウトする
function onEdit(e) {
//  Logger.log(e);
  myOnEdit(e); //ローカル動作時には、トリガー接続をせず、こっちのSimple Triggerから当該関数を呼び出す
}
//*/

/**
 * 編集が行われた場合に必ず自動的に呼びだされる関数。
 *   20190606 yybtcbk デフォルトのonEdit(e)だと、アノニマスユーザーの場合に動作していなかったので、
 *     Simple Trigger としてではなく、明示的に設定した installable trigger として呼び出すよう設定。
 *     cf https://stackoverflow.com/a/26318730/3799649 , https://stackoverflow.com/q/36871241/3799649
 * @param {EventObject} e - もらったイベントオブジェクト
 */
function myOnEdit(e) {
  try{
//    var eShtName = e.source.getActiveSheet().getName();
    var eShtIdx = e.source.getActiveSheet().getSheetId();

    if(eShtIdx == SHEETS_DICT.map.gid){ //マップシートの場合
      (new MapSheet()).processOnEdit(e);
    }
    else if(eShtIdx == SHEETS_DICT.lvlCalc.gid){ //レベルアップ金貨計算シートの場合
      (new LevelCalcSheet()).processOnEdit(e);
    }
  }catch(err){
    var errArr = err.stack.split("\n");
    errArr.unshift("[ERROR] "+err.name+": "+err.message);
    writeLog("onEdit",errArr);
    //since rethrowing err resets the stacktrace, work around this by appending current stacktrace to err.message (can't be bothered to properly address this nuisance...)
    errArr.shift();
    err.message += "\n(" + errArr.join("\n") + ")";
//Logger.log(err.message);    
    throw err; //rethrow for handling by Stackdriver Exception logging
  }
}



/*** OLDER CODES BELOW ***/
/*
//ベース： https://qiita.com/ringCurrent/items/3a3e82d1e854fdf80957

function setCurrentTime_R(value, index) {
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
  var str_time = sheet.getRange(index).getValue();
  var str_input = value;
  var temp = "";

  if ((value) && (str_time != "")) {
    var ctime = new Date();
    var hh = ctime.getHours();
    var mm = ctime.getMinutes();
    var t = hh+":"+mm;
    return t;
  } else {
    return temp;
  }
}
*/ //TODO ↓の関数使わなくなったらコメントアウトを広げる
//上記の改変：表示する日時のフォーマットを変更可能に
//　20190604 yybtcbk 勝手に呼ばれるので勝手に更新される、自身の値もうまく使えない、等で放棄
//　　　　　　　　　　　詳細：シートのロード時？、スクリプト編集時、等？に不定期に勝手に呼ばれて勝手に更新されるため、制御が難しく、
//　　　　　　　　　　　　　　また、更新中は、先の日付記述有無に関係なく、自身の値が「Loading...」となり、
//　　　　　　　　　　　　　　また、独自関数として使う場合、自身以外のセルに触れられず、
//　　　　　　　　　　　　　　また、別のセルを退避に使おうとも、それらも併せて「Loading...」となるので使えず、
//　　　　　　　　　　　　　　もう、面倒になってonEdit()に当該処理を差し込んだ。その方が楽。
function setCurrentTime_R_fmt(value, index) {
//var msg = "ctrf-start="+Utilities.formatDate(new Date(), "JST", "HH:mm:ss.SSS");
  //var sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
  //var range = SpreadsheetApp.getActiveRange();
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("マップ");//shtName);
  var range = sheet.getRange(index); //？？現在値のindexは固定で渡さないと、変な呼ばれ方をすると誤動作する？（シートも？）
//  var str_time = sheet.getRange(index).getValue(); //???
  var str_time = range.getValue(); //???
  var mycol = range.getColumn();
  var myrow = range.getRow();
  var ret = "";
  
  if ((value) && (str_time !== "")) {
    return Utilities.formatDate(new Date(), "JST", "M/d HH:mm"); //though "JST" is probably deprecated...?
    //ret = Utilities.formatDate(new Date(), "JST", "M/d HH:mm");
  } else {
    return "";
  }
//msg+="ctrf - end="+Utilities.formatDate(new Date(), "JST", "HH:mm:ss.SSS");ret=msg;
//  return ret;
}
//*/
