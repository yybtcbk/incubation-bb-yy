/**
 * レベルアップ金貨計算シートの関係処理用クラス
 *  20190605 yybtcbk 初版：編集時処理に対応
 *  20190606 yybtcbk データのセーブ・ロードに対応
 *  20190611 yybtcbk データのセーブ・ロード機能を調整（自動からチェックボックス起因に）
 */
var LevelCalcSheet = function(){
  //======= private vars and actions upon construction =======
//writeLog("dbg:LVC","LVC-create");
  var currSht;          //今のシート（レベルアップ金貨計算シート）
  var currNameRow = -1; //今の名前欄の行数値
  var currNameCol = -1; //今の名前欄の列数値
  var currCellId = ""   //今の編集セルに対応するセルID
  
  //======= privileged functions =======
  /**
   * 編集時処理
   *  20190605 yybtcbk 初版
   *    機能１：該当時（テーブルのVLOOKUPで取れなかった情報の入力時）に、該当データを自動ログとして残す。
   *      制限：マスタテーブルの自動更新まではやらない。手動で転記として、間で人の目での確認を取るようにする。（それでも手動報告よりは楽になる）
   *    機能２：名前クリア時に、手入力セルを全てクリアする。（名前欄から各セルへの位置関係は固定とする）
   *  20190606 yybtcbk データのセーブ・ロードに対応
   *    セーブ：名前クリア時に、マスタ上のセーブ用テーブルに、入力値の一部を退避する。（主キー：名前）
   *           同じ名前のデータ行を上書きし、また後で使えるように、タイムスタンプも付与する。
   *    ロード：名前入力・変更時に、セーブ用テーブルの名前列に同じ名前があれば、データロードを行うかの確認ダイアログを表示する。
   *           「はい」と押された場合のみ、セーブデータをロードして、既存入力を上書きする。
   *           また、対応して、セーブ用テーブルの名前列の情報を、名前入力の補助として使う。
   *  20190610 yybtcbk セーブ・ロードをチェックボックス起動に変更
   *    ・自動セーブ廃止については、セーブせずに気軽に使いたいユーザーにも対応したいため
   *    ・自動ロード廃止については、環境（スマホ、ブラウザ依存、ポップアップブロッカー系の有無）によっては動かず、しかも動かない場合、
   *      その時点で処理が止まってしまい、メインの処理に支障が出るため
   *    ・どちらの場合も、ユーザー操作を契機とした方が、使い勝手がよくなるが、ボタン動作だと権限問題が再浮上するので、逡巡していたが、
   *      チェックボックスならonEdit()が反応してくれるので、この対応とした。
   *  20190611 yybtcbk 公開前の微調整と、テストエリア→実動エリア３つへの転記対応
   * @param {EventObject} e - onEdit(e) 経由でもらったイベントオブジェクト
   */
  this.processOnEdit = function(e){
    currSht = e.source.getActiveSheet();
    var eRow = e.range.getRow();
    var eCol = e.range.getColumn();
//writeLog("dbg:LVC","cellA1="+e.range.getA1Notation()+",value="+e.range.getValue()+",oldValue="+e.oldValue);

    //＝＝＝マスタ修正履歴も残す
    if(eCol>=LevelCalcSheet.MASTER_START_COL){
      writeLog("lvlCalc","cellA1="+e.range.getA1Notation()+",value="+e.range.getValue()+",oldValue="+e.oldValue); //ログ出力
    }
    //＝＝＝データのセーブ・ロード用のチェックボックス押下時
    else if(doesEventMatchCellId_(eRow,eCol,"ckboxLoad")||doesEventMatchCellId_(eRow,eCol,"ckboxSave")){ //memo: short-circuits to give proper currName and currCellId values
      e.range.setValue(""); //reset relevant checkbox first
      var name = getValueFromCellId_("name");
      if(name==="") { //can't save nor load if name is empty
        var errmsg1 = (currCellId==="ckboxLoad" ? "ロード" : "セーブ");
        setValueFromCellId_("scriptMsg",errmsg1+"エラー：名前欄が空です。");
        return;
      }
      var saveRange = e.source.getRangeByName("lvlCalcSaveTable");//セーブ用テーブル
      var saveFullArr = saveRange.getValues();//セーブデータ二次元配列（空白行を含む）
      var saveArr = saveFullArr.filter(function(el){return el[0]!=="";});//セーブデータ二次元配列
      var saveNameMap = saveArr.map(function(el){return el[0];});//セーブ名前マップ
      var saveHeaders = saveArr[0]; //セーブ対象のセルIDを、テーブルのヘッダーから取得
      var cellarr, savedataArr;
      
      if(currCellId==="ckboxLoad"){ //データロード
        if(!saveNameMap.includes(name,1)){ //名前が未登録の場合                                  //start from idx=1 to skip header
          setValueFromCellId_("scriptMsg","ロードエラー：名前が未登録です。");
          return;
        }
//don't use popup msgBox, since it won't work for some users/browsers/mobile/etc.
//        var ui = SpreadsheetApp.getUi();
//        var res = ui.alert("保存データのロード", "前回入力したデータがあります。\nロードしますか？", ui.ButtonSet.YES_NO);
//        if(res == ui.Button.YES){ //「はい」が押されたら、データロード処理に入る
writeLog("dbg:LVC","Loading data for area with checkbox in cell "+e.range.getA1Notation()+", name="+name);
        setValueFromCellId_("scriptMsg","データのロード中……");
        savedataArr = saveArr.filter(function(el,idx){return ((idx>0)&&(el[0]===name));})[0];//セーブデータ配列 //specify idx>0 to skip header
        for(var i=0; i<saveHeaders.length; i++){
          if(saveHeaders[i]==="timestamp") continue; //don't (can't) load timestamp data
          setValueFromCellId_(saveHeaders[i],savedataArr[i]);
        }
//        }
        setValueFromCellId_("scriptMsg","データをロードしました。");
      }
      else if(currCellId==="ckboxSave"){ //データセーブ
writeLog("dbg:LVC","Saving data for area with checkbox in cell "+e.range.getA1Notation()+", name="+name);
        setValueFromCellId_("scriptMsg","データのセーブ中……");
        var saveIdx;
        savedataArr = new Array(saveHeaders.length);
        var searchName = ""; //名前が未登録の場合は、空欄を探すが、
        if(saveNameMap.includes(name,1)) searchName = name; //名前が登録済みの場合は、その行を使う                  //start from idx=1 to skip header
        for(saveIdx=1; saveIdx<saveFullArr.length; saveIdx++) if(saveFullArr[saveIdx][0]===searchName) break; //start from idx=1 to skip header
        //テーブル上限に達するとCell reference out of rangeになるので、
        //　とりあえずは最終行を上書きする仕様にして逃げる
        //TODO タイムスタンプの古い行を整理したり優先的に上書きしたりしたい
        if(saveIdx>=saveFullArr.length) saveIdx = saveFullArr.length-1;
        
        //get necessary data from input fields, and store as array
        for(var i=0; i<saveHeaders.length; i++){
          if(saveHeaders[i]==="timestamp") continue; //save timestamp data later
          savedataArr[i]=getValueFromCellId_(saveHeaders[i]);
        }
        //write out savedata
        var nameIdx = saveHeaders.indexOf("name");
        if(!savedataArr.every(function(el,idx){return ((el==="")||(idx==nameIdx));})){ //名前以外のデータがセットされていれば、通常書き出し
          //additionally write out timestamp
          savedataArr[saveHeaders.indexOf("timestamp")]=Utilities.formatDate(new Date(),"JST","YYYYMMddHHmmss");
          //write out data to save table
          //memo: [array[i,j]->range.getCell(1+i,1+j)]->range.offset(i,0,1,array.len)
          saveRange.offset(saveIdx,0,1,saveHeaders.length).setValues([savedataArr]);
          setValueFromCellId_("scriptMsg","データをセーブしました。");
        }
        else{ //名前以外のデータが空の状態でセーブされたら、該当する名前のセーブデータをテーブルから削除する
          if(searchName===""){ //ただし、セーブ済データが無い場合はこっち
            setValueFromCellId_("scriptMsg","（無効な操作のため、無視されます。）");
          }
          else{ //削除するデータがある場合はこっち
            //also clear name field data and timestamp field data
            savedataArr[nameIdx]="";
            savedataArr[saveHeaders.indexOf("timestamp")]="";
            //write out data to save table, as usual
            saveRange.offset(saveIdx,0,1,saveHeaders.length).setValues([savedataArr]); 
            setValueFromCellId_("scriptMsg","該当するデータを、セーブエリアから削除しました。");
          }
        }
      }
    }
    //＝＝＝名前欄のセルの編集時
    else if(doesEventMatchCellId_(eRow,eCol,"name")){
      var name;
      if(e.range.getValue()===""){ //名前消去時には、そのセクションの手入力セルを全てクリアしつつデータ保存
        name = e.oldValue; //消去前に入っていた名前を使う
writeLog("dbg:LVC","Name cleared in cell "+e.range.getA1Notation()+"; clearing corresponding inputs, name="+name);
        var cellarr;
        for(var cellId in LevelCalcSheet.LVC_DICT){
          if(LevelCalcSheet.LVC_DICT.hasOwnProperty(cellId)){
            cellarr = LevelCalcSheet.LVC_DICT[cellId];
            if(cellarr[0]<LevelCalcSheet.LVC_CELL_TYPE.calcStart){ //入力系セルの場合のみ
              if(cellId==="cashXpRatio"){ //特別ケース： TODO 金貨/経験値の倍率固定後に処理を抜く
                setValueFromCellId_(cellId,"5");
              }else{
                setValueFromCellId_(cellId,"");
              }
            }
          }
        }
      setValueFromCellId_("scriptMsg","名前を記入してください。"); //初期状態のメッセージ
      }
      else{//名前入力時
        //先に通常時のメッセージを表示しておく
        setValueFromCellId_("scriptMsg","指揮官レベルから、白枠に記入してください。");
        name = e.range.getValue();
        var saveRange = e.source.getRangeByName("lvlCalcSaveTable");//セーブ用テーブル
        var nameArrD = saveRange.offset(1,0,saveRange.getNumRows()-1,1).getValues(); //セーブ済みの名前の配列 [[nameA],[nameB],...]
        var nameArr = nameArrD.map(function(el){return el[0];});                     //セーブ済みの名前の配列 [nameA,nameB,...]
        if(nameArr.includes(name)){ //名前がセーブデータに登録済みの場合
          setValueFromCellId_("scriptMsg","（セーブデータがあります。ロード可能です。）");
        }
      }
    }
    //＝＝＝「次Lv必要Exp」の手入力セルの編集時
    else if(doesEventMatchCellId_(eRow,eCol,"inputNextXp")){
      var inputNextXp = e.range.getValue();
      if(isNumber(inputNextXp)){     //且つ（空ではない）数値入力時
        var lvl = getValueFromCellId_("level");
        if(isNumber(lvl)){ //現在の指揮官レベルに数値が入っていて、且つ
          if(getValueFromCellId_("vlNextXp")===""){ //「次Lv必要Exp」の自動ルックアップセルが空の場合
            var units = getValueFromCellId_("unitsNextXp");
            writeLog("lvlCalc","level="+lvl+",inputNextXp="+inputNextXp+units); //ログ出力
          }
        }
      }
    }
    //＝＝＝国税の住宅LvBUFF入力セル、もしくは徴収一回入手金貨額セルの編集時
    else if(doesEventMatchCellId_(eRow,eCol,"taxBuff")||doesEventMatchCellId_(eRow,eCol,"taxPerRun")){ //memo: short-circuits to give proper currName values
      var vlTaxBase = getValueFromCellId_("vlTaxBase");
      if(vlTaxBase===""){ //国税局基礎額（ルックアップ値）セルが空白の場合（ルックアップ失敗時）のみ、
        var inputTaxBuff = getValueFromCellId_("taxBuff");
        var inputTaxPerRun = getValueFromCellId_("taxPerRun");
        if(isNumber(inputTaxBuff) && isNumber(inputTaxPerRun)){ //各入力値が（空ではない）数値入力時
          var calcTaxBase = roundValToDp(getValueFromCellId_("calcTaxBase"),9); //割り算から浮動小数点演算誤差が出るので適当に丸める
          var units = getValueFromCellId_("unitsTaxBase");
          var lvl = getValueFromCellId_("level");
          writeLog("lvlCalc","level="+lvl+",taxBase="+calcTaxBase+units+"(taxPerRun="+inputTaxPerRun+units+",taxBuff=+"+inputTaxBuff+"%)"); //ログ出力
        }
      }
    }
  };
  //======= private functions =======
  /**
   * 渡されたイベントの行数値と列数値が、セルIDに対応するセルかどうかを判定し、対応する場合は、以下の設定も行う：
   *   1. currNameRowに対応する名前欄の行数値をセット
   *   2. currNameColに対応する名前欄の列数値をセット
   *   3. currCellIdに対応するセルIDをセット
   * @param {number} eRow - イベントの行数値
   * @param {number} eCol - イベントの列数値
   * @param {string} cellId - セルID（LVC_DICTのキー）
   * @return {boolean} 渡されたイベントの行数値と列数値が、セルIDに対応するセルかどうかの真偽値
   */  
  function doesEventMatchCellId_(eRow,eCol,cellId){
    //現在値から左上に十分に進めない場合を先に除外する
    if((eRow<=LevelCalcSheet.LVC_DICT[cellId][1])||(eCol<=LevelCalcSheet.LVC_DICT[cellId][2])) return false;
    //現在値がセルIDに対応する場合の、対応する名前欄の位置が、登録されているかを確認する
    
    //!!!TODO 高速化：この方式だと、座標比較のために、無駄にgetRange()が呼ばれて、 **とても非効率的** になっていると思われる。
    //!!!     なので、LVC_NAME_CELLSを、{row,col}形式のdictに変更し、データ内の計算だけで比較するように改善する。
    //!!!     ここの処理は、レベル計算シートの **編集動作全ての重さ** としてかかってくるため、最優先で調整したい。
    
    var nameCell = currSht.getRange(eRow-LevelCalcSheet.LVC_DICT[cellId][1],eCol-LevelCalcSheet.LVC_DICT[cellId][2]);
    var idx = LevelCalcSheet.LVC_NAME_CELLS.indexOf(nameCell.getA1Notation());
    if(idx==-1) return false;
    //以下、対応していた場合
    currNameRow = nameCell.getRow();
    currNameCol = nameCell.getColumn();
    currCellId = cellId;
    return true;
  }
  /**
   * 渡されたセルIDに対応するセルの値を返す。
   * 　前提：currNameRowとcurrNameColとcurrShtが、正しくセットされていること
   * @param {string} cellId - セルID（LVC_DICTのキー）
   * @return {string} 対応するセルのRangeオブジェクト、対応するセルIDがなければ空文字列
   */
  function getValueFromCellId_(cellId){
    if(!(cellId in LevelCalcSheet.LVC_DICT)) return "";
    return currSht.getRange(currNameRow+LevelCalcSheet.LVC_DICT[cellId][1],currNameCol+LevelCalcSheet.LVC_DICT[cellId][2]).getValue();
  }
  /**
   * 渡されたセルIDに対応するセルに値を書き込む。
   * 　前提：currNameRowとcurrNameColとcurrShtが、正しくセットされていること
   * @param {string} cellId - セルID（LVC_DICTのキー）
   * @param {string} value - そのセルに書き込む値
   */
  function setValueFromCellId_(cellId,value){
    if(!(cellId in LevelCalcSheet.LVC_DICT)) return "";
    return currSht.getRange(currNameRow+LevelCalcSheet.LVC_DICT[cellId][1],currNameCol+LevelCalcSheet.LVC_DICT[cellId][2]).setValue(value);
  } //TODO REWRITE USING THIS
  
};
//======= (static) members =======
/**
 * レベルアップ金貨計算シートの名前欄のセルの位置のA1ノーテーションの配列((static) Array)
 * @type {Array.<string>}
 */
LevelCalcSheet.LVC_NAME_CELLS = ["U4","B4","H4","N4",];
/**
 * レベルアップ金貨計算シートのセル種別定義辞書
 * @type {Object.<string,number>}
 */
LevelCalcSheet.LVC_CELL_TYPE = {
  //＝入力セル
  input:0,
  inputIfNecc:1,   //必要に応じて入力するセル
  inputByScript:2, //スクリプトによって書き込まれるセル（ユーザー編集可）
  ckbox:5,         //チェックボックス
  ckboxAction:6,   //チェックボックス（アクション用：ボタンの代わり：onEdit()を呼び出すために使用）
  //＝計算された値が入るセル
  calc:10,
  //＝ルックアップされた値が入るセル
  vlookup:20,
  //＝各種別の開始内部値
  inputStart:0,
  calcStart:10,
  vlookupStart:20,
};
/**
 * レベルアップ金貨計算シートの各種値セルの定義辞書((static) dictionary)
 * keyはセルID
 * valueの配列の内容は、
 *   [0]：セル種別
 *   [1]：名前セルからの行（Row）オフセット
 *   [2]：名前セルからの列（Column）オフセット
 * @typedef {Array.<number>} LvcDictArr
 * @type {Object.<string,LvcDictArr>}
 */
LevelCalcSheet.LVC_DICT = {
  name:              [LevelCalcSheet.LVC_CELL_TYPE.input,0,0], //名前
  scriptMsg:[LevelCalcSheet.LVC_CELL_TYPE.inputByScript,-2,0], //スクリプトの実行結果の表示
  ckboxSave:  [LevelCalcSheet.LVC_CELL_TYPE.ckboxAction,-1,3], //データセーブ起動用のチェックボックス
  ckboxLoad:   [LevelCalcSheet.LVC_CELL_TYPE.ckboxAction,0,3], //データロード起動用のチェックボックス
  level:             [LevelCalcSheet.LVC_CELL_TYPE.input,1,1], //現在の指揮官レベル
  nowCash:           [LevelCalcSheet.LVC_CELL_TYPE.input,3,1], //現在の所持金貨
  nowXp:             [LevelCalcSheet.LVC_CELL_TYPE.input,5,1], //現在のExp
  vlNextXp:        [LevelCalcSheet.LVC_CELL_TYPE.vlookup,7,1], //「次Lv必要Exp」（ルックアップ値）
  inputNextXp: [LevelCalcSheet.LVC_CELL_TYPE.inputIfNecc,8,1], //「次Lv必要Exp」の手入力（必要なら）
  unitsNextXp:     [LevelCalcSheet.LVC_CELL_TYPE.vlookup,8,2], //Exp系の単位
  xpToNext:          [LevelCalcSheet.LVC_CELL_TYPE.calc,10,1], //次LvまでのExp
  cashXpRatio:      [LevelCalcSheet.LVC_CELL_TYPE.input,12,1], //金貨/経験値の倍率     //TODO 検証で固まったらcalcタイプに変更する
  cashToNext:        [LevelCalcSheet.LVC_CELL_TYPE.calc,14,1], //次Lvまでの金貨
  unitsTaxBase:   [LevelCalcSheet.LVC_CELL_TYPE.vlookup,17,2], //国税局基礎額などの単位
  vlTaxBase:      [LevelCalcSheet.LVC_CELL_TYPE.vlookup,17,1], //国税局基礎額（ルックアップ値）
  taxBuff:    [LevelCalcSheet.LVC_CELL_TYPE.inputIfNecc,19,1], //国税局：住宅LvBUFFの手入力（必要なら）
  taxPerRun:  [LevelCalcSheet.LVC_CELL_TYPE.inputIfNecc,21,1], //国税局：1回入手金貨の手入力（必要なら）
  calcTaxBase:       [LevelCalcSheet.LVC_CELL_TYPE.calc,24,1], //国税局基礎額（計算値）
  cashAddReq:        [LevelCalcSheet.LVC_CELL_TYPE.calc,26,1], //必要金貨
  chestsReq:         [LevelCalcSheet.LVC_CELL_TYPE.calc,29,1], //必要金貨が入手できるまでの宝箱の数
  nowChestNum:      [LevelCalcSheet.LVC_CELL_TYPE.input,31,1], //所持金貨バンバン宝箱
  cashFromChests:    [LevelCalcSheet.LVC_CELL_TYPE.calc,33,1], //開封時の入手金貨
};
/**
 * レベルアップ金貨計算シートのマスタエリア開始列数
 * @type {number}
 */
LevelCalcSheet.MASTER_START_COL = 29;
